#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, CHAR, DOUBLE};

TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; 

map<string, enum TYPES> DeclaredVariables;	// Store declared variables and their types
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
enum TYPES Expression(void);			// Called by Term() and calls Term()
void StatementPart(void);
		
enum TYPES Identifier(void){
	enum TYPES t;
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	t=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return t;
}

enum TYPES Number(void){
	double f;					
	unsigned int *i;
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){

		f=atof(lexer->YYText());
		i=(unsigned int *) &f; // i points to the const double
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<f<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<f<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{ 
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	
}

enum TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

enum TYPES Factor(void){
	enum TYPES t;
	if(current== RPARENT){
			current=(TOKEN) lexer->yylex();
			t=Expression();
			if(current!=LPARENT)
				Error("')' était attendu");
			else
				current=(TOKEN) lexer->yylex();
			}
		else if(current== NUMBER)
			t=Number();
		else if(current== ID)
			t=Identifier();
		else if(current== CHARCONST)
			t=CharConst();
		else
			Error("'(', ou constante ou variable attendue.");
	return t;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	OPMUL mulop;
	enum TYPES t=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		
		enum TYPES t2=Factor();
		if(t2!=t)
			Error("types incompatibles dans l'expression");
		switch(mulop){
			case AND:
				if(t2!=BOOLEAN)
					Error("type non booléen pour l'opérateur AND");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;
				cout << "\tpush %rax\t# AND"<<endl;	
				break;
			case MUL:
				if(t2!=INTEGER and t2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(t2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	
					cout << "\tpush %rax\t# MUL"<<endl;
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(t2!=INTEGER and t2!=DOUBLE)
					Error("type non numérique pour la division");
				if(t2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;		// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;	
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(t2!=INTEGER)
					Error("type non entier pour le modulo");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return t;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	OPADD adop;
	enum TYPES t=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();
		enum TYPES t2=Term();
		if(t2!=t)
			Error("les types sont incompatibles");
		switch(adop){
			case OR:
				if(t2!=BOOLEAN)
					Error("opérande non booléenne pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;
				cout << "\tpush %rax"<<endl;	
				break;			
			case ADD:
				if(t2!=INTEGER and t2!=DOUBLE)
					Error("l'opérande doit être de type 'INTEGER' ou 'DOUBLE' pour l'addition");
				if(t2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	
					cout << "\tpush %rax"<<endl;
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;			
			case SUB:	
				if(t2!=INTEGER and t2!=DOUBLE)
					Error("l'opérande doit être de type 'INTEGER' ou 'DOUBLE' pour la soustraction");
				if(t2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	
					cout << "\tpush %rax"<<endl;
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("opérateur additif inconnu");
		}
	}
	return t;
}

enum TYPES Type(void){

	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else
		Error("type inconnu");
	
}

// Declaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> ident;  //stockage des indentificateurs
	if(current!=ID)
		Error("Un identificateur était attendu");
	ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		ident.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	enum TYPES t=Type();
	for (const auto& elem : ident) {
		if(t==INTEGER or t==BOOLEAN)
			cout<<elem<<":\t.quad 0"<<endl;
		else if(t==DOUBLE)
			cout<<elem<<":\t.double 0.0"<<endl;
		else if(t==CHAR)
			cout<<elem<<":\t.byte 0"<<endl;
		else Error("type pas reconnu");
		DeclaredVariables[elem]=t;
	}	
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){

	unsigned long long tag;
	OPREL oprel;
	enum TYPES t=SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel=RelationalOperator();
		enum TYPES t2=SimpleExpression();
		if(t2!=t)
			Error("les types sont incompatibles pour la comparaison");
		if(t!=DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<tag<<endl;
		cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<tag<<":"<<endl;
		return BOOLEAN;
	}
	return t;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	enum TYPES t=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	enum TYPES t2=Expression();
	if(t2!=t){
		cerr<<"Type variable "<<t<<endl;
		cerr<<"Type Expression "<<t2<<endl;
		Error("les types sont incompatibles dans l'affectation");
	}
	if(t==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else
		cout << "\tpop "<<variable<<endl;
}

void Statement(void);

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	enum TYPES t=Expression();
	if(t==INTEGER){
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else if(t== BOOLEAN){
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
		}
	else if (t== DOUBLE){
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
		}
	else if(t== CHAR){
			cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
		}
	else
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");

}

// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void){
	current=(TOKEN) lexer->yylex();
	cout<<"FOR: ";
	
	enum TYPES type1, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type1){
		cerr<<"Type variable "<<type1<<endl;
		cerr<<"Type Expression "<<type2<<endl;
		Error("types incompatibles dans l'affectation");
	}
	if(type1==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
		cout << "\tpop %rcx "<<endl;
	if(strcmp(lexer->YYText(),"TO")==0){
		cout<<lexer->YYText()<<": ";
	current=(TOKEN) lexer->yylex();
	Expression();
	cout<<"\tpop %rdx\t# Get the result of expression"<<endl;
	cout<<"\tcmpq %rdx, %rcx"<<endl;
	cout<<"\tja finFor"<<endl;
	
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"addq $1, %rcx\t#a++"<<endl;
	cout<<"\tjmp TO"<<endl;}
	else if(strcmp(lexer->YYText(),"DOWNTO")==0){
		cout<<lexer->YYText()<<": ";
	current=(TOKEN) lexer->yylex();
	Expression();
	cout<<"\tpop %rdx\t# Get the result of expression"<<endl;
	cout<<"\tcmpq %rdx, %rcx"<<endl;
	cout<<"\tjb finFor"<<endl;
	
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"subq $1, %rcx\t#a--"<<endl;
	cout<<"\tjmp DOWNTO"<<endl;
}
	

	cout<<"finFor :"<<endl;
}

//WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"WHILE"<<tag<<":"<<endl;
	current = (TOKEN)lexer->yylex();

	if(Expression()!=BOOLEAN)
		Error("le type de l'expression doit être 'BOOLEAN'");
	cout<<"\tpop %rax\t# le resultat de l'expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje FinWhile"<<tag<<"\t# if FALSE, on sort de la boucle"<<endl;
	
	if (current==KEYWORD||strcmp(lexer->YYText(), "DO") == 0){
	cout<<lexer->YYText()<<": ";    //DO:
	current = (TOKEN)lexer->yylex();
	Statement();
	cout << "\tjmp WHILE"<<tag<< endl;}
	else Error("mot clé 'DO' attendu");
	
	cout<<"FinWhile"<<tag<<":"<<endl;
	
	}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"BEGIN"<<tag<<":"<<endl;	
	current=(TOKEN) lexer->yylex();
	Statement();

	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current==KEYWORD||strcmp(lexer->YYText(),"END")==0)
		current=(TOKEN) lexer->yylex();
	else Error("mot clé 'END'");
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	cout<<"IF: ";
	unsigned long  long tag=TagNumber++;
	current = (TOKEN)lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("l'expression doit être de type 'BOOLEAN'");
	cout<<"\tpop %rax\t# le resultat de l'expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje ELSE"<<tag<<"\t# if FALSE, jump to Else"<<tag<<endl;
	
	if (current==KEYWORD||strcmp(lexer->YYText(), "THEN") == 0){
		cout<<"THEN"<<tag<<": ";    
		current = (TOKEN)lexer->yylex();
		Statement();
		cout << "\tjmp FinIf" << tag << endl;
}
	else Error("mot clé 'THEN' attendu");
	
	cout << "ELSE" << tag << ": "<<endl;
	if (current==KEYWORD||strcmp(lexer->YYText(), "ELSE") == 0){
		current = (TOKEN)lexer->yylex();
		Statement();
	}
}

//RepeatStatement:= "repeat" Statement {; Statement} "until" Expression
void RepeatStatement(void){
	cout<<"Repeat:"<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();	// Skip the ";"
		Statement();
		
	}
		cout<<"UNTIL:"<<endl;
	if(current!=KEYWORD||strcmp(lexer->YYText(), "UNTIL")!=0)
		Error("mot-clé UNTIL attendu");
	current=(TOKEN) lexer->yylex();
	
	if(Expression()!=BOOLEAN)
		Error("l'expression n'est pas de type BOOLEAN");
	
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje EndRepeat"<<endl;
	cout<<"\tjmp Repeat"<<endl;
	
	cout<<"EndRepeat:"<<endl;
	current=(TOKEN) lexer->yylex();
}

//Empty:=
void empty(void){
	}

//Constant :=Number|Identifier|Charconst
enum TYPES Constant(void){
	
	enum TYPES type;
	
	switch(current){
		case NUMBER:
			type=Number();		
			break;
		case ID:
			type=Identifier();
			break;
		case CHARCONST:
			type=CharConst();
			break;
		default:
			Error("constante  attendue.");
	};
	cout<<"\tpop %rdx"<<endl;
	cout<<"\tcmpq %rcx, %rdx"<<endl;
	
	return type;
}

unsigned long long tagC=0;  //compteur des constantes sur une ligne

//CaseLabelList := Constant {, Constant }
void CaseLabelList(void){
	unsigned long long tag3=tagC++;
	unsigned long long tag4=TagNumber-2;    //compteur des instructions (statement)
		
	Constant();
	
	while(current==COMMA){
		tag3++;
		cout<<"\tjne constante"<<tag3<<endl;
		cout<<"\tjmp instruction"<<tag4<<endl;
		current=(TOKEN) lexer->yylex();
		cout<<"constante"<<tag3<<":"<<endl;
		Constant();
			}
}

//CaseListElement := CaseLabelList : Statement> | Empty
void CaseListElement(void){
	unsigned long long tag2=TagNumber++;
	unsigned long long tag=tag2-1;   //compteur des instructions
	
	
	CaseLabelList();
	cout<<"\tjne Cas"<<tag2<<"\t# if FALSE, jump to Cas"<<endl;
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"instruction"<<tag<<":"<<endl;
	if(current==SEMICOLON or strcmp(lexer->YYText(),"END")==0)
		empty();	
	else		
	Statement();	
}	



//CaseStatement> := "case" Expression "of" CaseListElement {; CaseListElement} "end"
void CaseStatement(void){
	unsigned long long constante=0;
	unsigned long long tag=TagNumber++;
	cout<<"Case:"<<endl;
	current=(TOKEN) lexer->yylex();
		if(Expression()!=INTEGER and Expression()!=BOOLEAN and Expression()!=DOUBLE)
			Error("le type de l'expression est inconnu");
		
	cout<<"\tpop %rcx\t# Get the result of expression"<<endl;

	if(current!=KEYWORD||strcmp(lexer->YYText(),"OF")!=0)
		Error("caractère 'OF' attendu");
			
	current=(TOKEN) lexer->yylex();
	
	cout<<"Cas"<<tag<<":"<<endl;
	
	tag++;
	
	CaseListElement();
	cout<<"\tjmp FinCas"<<endl;
	
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		cout<<"Cas"<<tag<<":"<<endl;
		CaseListElement();
		cout<<"\tjmp FinCas"<<endl;
	tag++;
}	
		cout<<"Cas"<<tag<<":"<<endl;

	if(current!=KEYWORD||strcmp(lexer->YYText(),"END")!=0)
		Error("caractère 'END' attendu");
	current=(TOKEN) lexer->yylex();	
	cout<<"FinCas:"<<endl;
}

void FieldList(void);

//RecordSection> := Identifier {, FieldList} : Type | Empty
void RecordSection(void){
	set<string> ident;   //stockage des identifiants
	if(current!=ID)
		Error("Un identifiant est attendu");	
	ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Identificateur attendu");
		FieldList();
		ident.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	
	current=(TOKEN) lexer->yylex();
	
	if(current==SEMICOLON or strcmp(lexer->YYText(),"END")==0)
		empty();
	else{
	enum TYPES t=Type();
	for (const auto& elem : ident) {
		if(t==INTEGER or t==BOOLEAN)
			cout<<elem<<":\t.quad 0"<<endl;
		else if(t==DOUBLE)
			cout<<elem<<":\t.double 0.0"<<endl;
		else if(t==CHAR)
			cout<<elem<<":\t.byte 0"<<endl;
		else Error("type pas reconnu");
		DeclaredVariables[elem]=t;
	}
}

}

//FixedPart:= RecordSection {;RecordSection}
void FixedPart(void){
	RecordSection();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		if(current==ID)  
			RecordSection();
		else break; 
	}	
}

//FieldList:= FixedPart | FixedPart ; CaseStatement | CaseStatement
void FieldList(void){
	if(current==ID){
		FixedPart();      // FieldList:= FixedPart
		if(strcmp(lexer->YYText(),"CASE")==0)   //FieldList:= FixedPart ; CaseStatement
			CaseStatement();
		}
	else if(strcmp(lexer->YYText(),"CASE")==0)			//FieldList:=CaseStatement
		CaseStatement();
	else Error("erreur: structure non reconnu");
}

//RecordType> ::= "record" FieldList "end"
void RecordType(void){
	cout<<"RECORD:"<<endl;
	current=(TOKEN) lexer->yylex();
	FieldList();
	
	if(strcmp(lexer->YYText(),"END")!=0)
		Error("mot clé 'END' attendu");
	current=(TOKEN) lexer->yylex();
	}

//Statement := IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | RepeatStatement | CaseStatement | AssignementStatement | RecordType
void Statement(void){
	if(current==KEYWORD and strcmp(lexer->YYText(),"IF")==0)
		IfStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"WHILE")==0)
		WhileStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"FOR")==0)
		ForStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"BEGIN")==0)
		BlockStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"DISPLAY")==0)
		DisplayStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"REPEAT")==0)
		RepeatStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"CASE")==0)
		CaseStatement();
	else if(current==KEYWORD and strcmp(lexer->YYText(),"RECORD")==0)
		RecordType();
	else if(current==ID)
		AssignementStatement();
	else 
		Error("un mot clé est attendu");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8"<<endl;	// Alignement on addresses that are a multiple of 8 (64 bits = 8 bytes)
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD and strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
