//MINI-PROJET ASSEMBLEUR DE: ET TAOUFIK YASSMINE

//				TP3	
// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]	
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement

//             TP4
//Modification des fonctions suivantes:
//Identifier
//Number
//Factor
//Term
//SimpleExpression
//Expression
//AssignementStatement
//IfStatement
//WhileStatement

//				TP5	
// DisplayAssignement := "DISPLAY" Expression

//                TP6 
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type

//					TP7
//modification de :
//Factor
//Term
//SimpleExpression
//Type
//VarDeclaration
//Expression

//				TP8
//RepeatStatement:= "repeat" Statement {; Statement} "until" Expression

//CaseStatement := "case" Expression "of" CaseListElement {; CaseListElement} "end"
//CaseListElement := CaseLabelList : Statement> | Empty
//CaseLabelList := Constant {, Constant }
//Constant :=Number|Identifier|Charconst
//Empty:=

//RecordType> ::= "record" FieldList "end"
//FieldList:= FixedPart | FixedPart ; CaseStatement | CaseStatement
//FixedPart:= RecordSection {;RecordSection}
///RecordSection> := Identifier {, FieldList} : Type | Empty

