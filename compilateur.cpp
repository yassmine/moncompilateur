//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

unsigned long TagNumber=0;

char current, lookedAhead;                // Current char    
int NLookedAhead=0;

void ReadChar(void){
    if(NLookedAhead>0){
        current=lookedAhead;    // Char has already been read
        NLookedAhead--;
    }
    else
        // Read character and skip spaces until 
        // non space character is read
        while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
    NLookedAhead++;
}


void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Factor := Number | Letter | "(" Expression ")"| "!" Factor

enum OPREL {equ, diff, infe, supe, inf, sup, unknown};

	
void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}
		
void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void Number(void){
	unsigned long long number;
	if((current<'0')||(current>'9'))
		Error("chiffre attendu");		   // Digit expected
	else
		number=current-'0';
	ReadChar();
	while(current>='0' && current <='9'){
		number*=10;
		number+=current-'0';
		ReadChar();
	}
	cout <<"\tpush $"<<number<<endl;
}



void Letter(void){
	if((current<'a')||(current>'z'))
		Error("lettre attendue");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		cout << "\tpush "<<current<<endl;
		ReadChar();
	}
}


void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Digit();
	     	else
			Error("'(' ou chiffre attendu");
}

void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}

}

OPREL RelationOperator(void){
	if(current!='<'&&current!='>'&&current!='!'&&current!='=')
		return unknown;
	LookAhead();
	if(lookedAhead=='='){
		if(current=='<'){
			ReadChar();
			ReadChar();
			return infe;
		}
		else {
			ReadChar();
			ReadChar();
			return supe;
		}
	}
	
	else if( lookedAhead=='>'){
		if(current=='<'){
			ReadChar();
			ReadChar();
			return diff;
		}
	}
	
	if(current=='='){
		ReadChar();
		return equ;
	}
	else if(current=='<'){
		ReadChar();
		return inf;
	}
	else if(current=='>'){
		ReadChar();
	return sup;
	}
	else {
		ReadChar();
		return unknown	;	
	}
}
		
void Expression (void){
	OPREL op;
	ArithmeticExpression();
	if(current=='=' || current=='<'||current== '>' ){
		op=RelationOperator();
		ArithmeticExpression();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		cout<<"\tcmpq %rax %rbx"<<endl; //comparer rax et rbx
		if(op==equ)
			cout<<"\tje then"<<++TagNumber<<"\t# If equal"<<endl;
		else if(op==diff)
			cout<<"\tjne then"<<++TagNumber<<"\t# If different"<<endl;
		else if(op==infe)
			cout<<"\tjbe then"<<++TagNumber<<"\t# If below or equal"<<endl;
		else if(op==supe)
			cout<<"\tjae then"<<++TagNumber<<"\t# If above or equal"<<endl;
		else if(op==inf)
			cout<<"\tjnae then"<<++TagNumber<<"\t# If below"<<endl;
		else if(op==sup)
			cout<<"\tjnbe then"<<++TagNumber<<"\t# If above"<<endl;
		else Error("unknow operator");
			cout << "\tpush $0\t\t# False"<<endl;
	cout << "\tjmp Suite"<<TagNumber<<endl;		
	cout << "Then"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
	cout << "Suite"<<TagNumber<<":"<<endl;
		
	}

	}	
	
void Factor(void){
	if(current=='('){
		ReadChar();
		Expression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Number();
	     	else
				if(current>='a' && current <='z')
					Letter();
				else
					Error("'(' ou chiffre ou lettre attendue");
}

// AssignementStatement =      Letter "=" Expression
void declarationOfAssignment(void){
	char lettre;
	if(current<'a' or current>'z'){
		Error("lettre entre a et z attendu");
	lettre=current;
	ReadChar();
	if(current!='=')
		Error("caractère '=' attendu");
	ReadChar();
	Expression();
	cout << "\tpop "<<lettre<<endl;
}
}

// StatementPart := Statement {";" Statement} "."
void instructionPart(void){
	declarationOfAssignment();
	while (current==';'){
		ReadChar();
		declarationOfAssignment();
	}
	if(current!='.')
		Error("caractère . attendu");
	ReadChar();
}

	
		

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
		Expression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

	

}
		
			





